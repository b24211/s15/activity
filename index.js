
let firstname = 'John';
console.log('First Name: ' + firstname);

let lastname = 'Smith';
console.log('Last Name: ' + lastname);

let age = 30;
console.log('Age: ' + age);

let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
console.log('Hobbies: ');
console.log(hobbies);

let workaddress = {
	houseNumber: 32,
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska'
}
console.log("Work Address: ")
console.log(workaddress);


	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let full_Name = "Bucky Barnes";
	console.log("My bestfriend is: " + full_Name);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);


